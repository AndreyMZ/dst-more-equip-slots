return {
    HEAVY    = "heavy",
    BACKPACK = "backpack",
    BAND     = "band",
    SHELL    = "shell",
    LIFEVEST = "lifevest",
    ARMOR    = "armor",
    DRESS    = "dress",
    AMULET   = "amulet",
    COSTUME  = "costume",
}
