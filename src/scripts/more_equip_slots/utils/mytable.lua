local mytable = {}

function mytable.invert(t)
    local result = {}
    for k, v in pairs(t) do
        local values = result[v]
        if values == nil then
            values = {}
            result[v] = values
        end
        table.insert(values, k)
    end
    return result
end

function mytable.contains_all(t, elements)
    for _, v in pairs(elements) do
        if not table.contains(t, v) then
            return false
        end
    end
    return true
end

function mytable.index_of(t, value)
    for i, v in ipairs(t) do
        if v == value then
            return i
        end
    end
    return nil
end

function mytable.light_copy(t)
    local result = {}
    setmetatable(result, { __index = t })
    return result
end

return mytable
