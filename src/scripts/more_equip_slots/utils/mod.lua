local mod = {}

local log = require "more_equip_slots/utils/log"


function mod.override_method(classdef, method, func)
    local super = classdef[method]
    classdef[method] = function(...)
        return func(super, ...)
    end
end


function mod.set_upvalue(fn, upvalue, builder)
    local i = 1
    while true do
        local upvalue_name, upvalue_value = debug.getupvalue(fn, i)
        if upvalue_name == nil then
            log.warning("Up-value not found: %s", upvalue)
            break
        elseif upvalue_name == upvalue then
            debug.setupvalue(fn, i, builder(upvalue_value))
            break
        end
        i = i + 1
    end
end


function mod.override_upvalue(fn, upvalue, func)
    mod.set_upvalue(fn, upvalue, function(orig)
        return function(...)
            return func(orig, ...)
        end
    end)
end


function mod.with_override(obj, key, new_value, func, ...)
    local orig_value = obj[key]
    obj[key] = new_value
    local result = {func(...)}
    obj[key] = orig_value
    return unpack(result)
end


function mod.with_slot(inventory, old_slot, new_slot, func, ...)
    local GetEquippedItem_Orig = inventory.GetEquippedItem
    inventory.GetEquippedItem = function(inst, eslot)
        if eslot == old_slot then
            eslot = new_slot
        end
        return GetEquippedItem_Orig(inst, eslot)
    end

    local Unequip_Orig = inventory.Unequip
    inventory.Unequip = function(inst, eslot, slip)
        if eslot == old_slot then
            eslot = new_slot
        end
        return Unequip_Orig(inst, eslot, slip)
    end

    func(...)

    inventory.GetEquippedItem = GetEquippedItem_Orig
    inventory.Unequip = Unequip_Orig
end


function mod.with_all_slots(inventory, old_slot, new_slots, func, ...)
    local GetEquippedItem_Orig = inventory.GetEquippedItem
    inventory.GetEquippedItem = function(inst, eslot)
        if eslot == old_slot then
            for _, new_slot in ipairs(new_slots) do
                local result = GetEquippedItem_Orig(inst, new_slot)
                if result ~= nil then
                    return result
                end
            end
            return nil
        else
            return GetEquippedItem_Orig(inst, eslot)
        end
    end

    local SwapEquipment_Orig = inventory.SwapEquipment
    inventory.SwapEquipment = function(inst, eslot)
        if eslot == old_slot then
            local result = false
            for _, new_slot in ipairs(new_slots) do
                result = SwapEquipment_Orig(inst, new_slot) or result
            end
            return result
        else
            return SwapEquipment_Orig(inst, eslot)
        end
    end

    func(...)

    inventory.GetEquippedItem = GetEquippedItem_Orig
    inventory.SwapEquipment = SwapEquipment_Orig
end


function mod.proxy(original, override)
    setmetatable(override, {
        __index = function(proxy, key)
            local result = original[key]
            if type(result) == "function" then
                return function(self, ...)
                    result((self == proxy and original or self), ...)
                end
            else
                return result
            end
        end,
        __newindex = original,
    })
    return override
end


return mod
