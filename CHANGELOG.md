1.3.6 (2024-05-11)

- Fix the bug [#15](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/15): Crash when *Void Cowl* is crafted.
- Fix repairable items after repairing.
- Add support for the *W.A.R.B.I.S. Armor*.

1.3.5 (2023-06-16)

- Add support for the *Void Robe*.

1.3.4 (2023-04-30)

- Add support for the *Brightshade Armor*.
- Add support for the *Punching Bag*, *Shadow Boxer*, and *Bright Boxer*.

1.3.3 (2023-04-18)

- Fix the bug [#10](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/10): Wrong positioning slots of *Enlightened Crown* and *Turf-Raiser Helm*.
- Fix the position of the *Bone Armor* fuel slot added by the *Item Auto-Fuel Slots* mod.
- Add support for the pillow armors.
- Add support for the *Dreadstone Armor*.

1.3.2 (2022-12-24)

- Add support for the mod [Item Auto-Fuel Slots](https://steamcommunity.com/sharedfiles/filedetails/?id=2736043172).
- Fix the bug [#9](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/9): Broken inventory bar UI.

1.3.1 (2022-11-20)

- Fix the bug [#8](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/8): Crash when getting near the stage.

1.3.0 (2022-11-15)

- Add support for the costumes.
- Add support for the *Mannequin*.
- Fix the bug [#6](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/6): Life Giving Amulet was not consumed on resurrect.

1.2.1 (2022-07-31)

- Fix the bug [#4](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/4): modmain.lua:342: attempt to index field 'owner' (a nil value).

1.2.0 (2022-07-23)

- Add Chinese translation.
- Add configuration options to automatically detect backpacks, armor, and dress from mods.
- Add a configuration option to automatically detect amulets from mods compatible with the mod "Extra Equip Slots".
- Add support for the mod [Functional Medal](https://steamcommunity.com/sharedfiles/filedetails/?id=1909182187).
- Add support for the mod [Island Adventures](https://steamcommunity.com/sharedfiles/filedetails/?id=1467214795).
- Add support for the mod [[DST] WhaRang](https://steamcommunity.com/sharedfiles/filedetails/?id=1108032281).
- Add support for the mod [Aria Crystal (Rebuild)](https://steamcommunity.com/sharedfiles/filedetails/?id=2418617371).
- Fix rendering of *One-man Band* (it was broken somewhere between DST versions 487848 and 503635).
- Fix rendering of *Chess Pieses* (sculptures).
- Rework the rendering mechanism.

1.1.5 (2022-05-12)

- Add support for the new item added with the updates: *Potato Sack*.
- Fix showing the *Construction Amulet* in the new crafting menu added with the updates.
- Fix the bug: Rotten Giant Crops were not rendered properly.

1.1.4 (2021-09-11)

- Add support for the new items added with the updates: *Chirpy Scarf*, *Chirpy Cloak*, *Chirpy Capelet*, *Knobbly Tree 
  Nut*.
- Fix the bug: chess pieces were not rendered properly.

1.1.3 (2021-05-27)

- Add support for the mod [More Armor](https://steamcommunity.com/sharedfiles/filedetails/?id=1153998909).
- Fix rendering of *Inflatable Vest* added with the update "Wes's Entirely Real Character Refresh".

1.1.2 (2021-02-24)

- Fix the bug [#1](https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues/1): Items with a skin are not rendered
