# Tools
- Klei Studio (TEXTool + TEXCreator): <https://github.com/HandsomeMatt/dont-starve-tools>
- ktools (ktech + krane): <https://github.com/nsimplex/ktools>
- BinaryConverter: <https://forums.kleientertainment.com/files/file/226-animation-commandline-tool/>

# Useful links
- Steam > Don't Starve Together > Workshop: <https://steamcommunity.com/app/322330/workshop/>
- Steam Workshop Downloader: <https://steamworkshopdownloader.io>
- Don't Starve Wiki > Console > Don't Starve Together Commands: <https://dontstarve.fandom.com/wiki/Console/Don't_Starve_Together_Commands>

# Useful console commands
~~~
c_save()
c_godmode()
c_goadventuring()
c_give("cane",1)
c_give("amulet",1)
c_spawn("chesspiece_hornucopia_stone",1)
c_gonext("charlie_stage")
c_gonext("multiplayer_portal")

ConsoleWorldEntityUnderMouse():Remove()

c_select()
print(c_sel().prefab)

print(ConsoleWorldEntityUnderMouse().prefab)
print(ConsoleCommandPlayer().replica.inventory:GetActiveItem().prefab)
print(ConsoleCommandPlayer().replica.inventory:GetItemInSlot(1).prefab)
print(ConsoleCommandPlayer().replica.inventory:GetEquippedItem(EQUIPSLOTS.HAND).prefab)
print(ConsoleCommandPlayer().replica.inventory:GetEquippedItem(EQUIPSLOTS.BODY).prefab)
print(ConsoleCommandPlayer().replica.inventory:GetEquippedItem(EQUIPSLOTS.HEAD).prefab)
~~~
