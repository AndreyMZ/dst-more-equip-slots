# Table of content

- [For other mod developers](#for-other-mod-developers)
- [For potential contributors](#for-potential-contributors)
- [For regular users](#for-regular-users)


# For other mod developers

If you are a developer of a mod which adds a custom body item, and you want to make your mode compatible with "More 
equip slots", then this guide is for you.

Let's assume that your item is named "Foo", the prefab is named `foo`, and it is an armor. Most probaly you have a file
`scripts/prefabs/foo_file.lua` with the content like this:
~~~lua
-- [...]

local function fn()
    -- [...]
    if not TheWorld.ismastersim then
        return inst
    end
    -- [...]
    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
    -- [...]
    return inst
end

return Prefab("foo", fn, assets)
~~~

Then you need to add one line:
~~~lua 
inst:AddTag("armor")
~~~
(For equip types other than armor use another value from the list [equip_types.lua][].)

So, your new code would be:
~~~lua
-- [...]

local function fn()
    -- [...]
    inst:AddTag("armor") -- <== NEW LINE
    -- [...]
    if not TheWorld.ismastersim then
        return inst
    end
    -- [...]
    inst:AddComponent("equippable")
    inst.components.equippable.equipslot = EQUIPSLOTS.BODY
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
    -- [...]
    return inst
end

return Prefab("foo", fn, assets)
~~~

Besides, ensure that you NEVER try to check if your item is equipped into the slot `EQUIPSLOTS.BODY`. Such code will not
work correcly when the item is equipped into one of the extra slots! Instead, check all equip slots.

BAD:
~~~lua
local function fooIsEquipped(inventory)
    local item = inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.BODY) -- <== BAD
    return item and item.prefab == "foo"
end
~~~

GOOD:
~~~lua
local function fooIsEquipped(inventory)
    for _, item in pairs(inventory.equipslots) do
        if item.prefab == "foo" then
            return true
        end
    end
    return false
end
~~~

That's all!


# For potential contributors

If you are a programmer, but you have no access to the incompatible mod, then you may contribute to "More equip slots"
instead. 

1. Clone [dst-more-equip-slots][].
2. Find out the *Prefab name* of the item (e.g. `foo`). You may use the [console][] command

        print(ConsoleCommandPlayer().replica.inventory:GetEquippedItem(EQUIPSLOTS.BODY).prefab)

    or

        print(ConsoleWorldEntityUnderMouse().prefab)

3. Select the suitable *equip type* from the file [equip_types.lua], e.g. `EQUIP_TYPES.ARMOR`.
4. Make the following changes:
    1. Add the key-value pair to the file [items_equip_types.lua][], e.g.:
        
            ["foo"] = EQUIP_TYPES.ARMOR, -- Foo
 
    2. Add the mod to the list of known compatible mods in the file [README.md][].
    3. Add a corresponding entry to the change log in the file [CHANGELOG.md][].
5. Check that the mods work OK with these changes. You may use [mklinks.bat] to install the local version of "More equip
   slots".
8. Make a new [merge request][].

That's all! I will merge the pull request and release a next version of "More equip slots" in a while.


# For regular users

If you do not understand the previous two sections, then you may just go to [issues][] and create a new issue. Please,
specify the following information (preferably in English):
1. The link to the incompatible mod in Steam Workshop.
2. The names of the items which this mod adds.

That's all! I will be notified of the new issue. If it is possible, I will fix it when I have time. Please do not report issues by
comments in Steam Workshop, because I won't be notified of them. 


[dst-more-equip-slots]:   https://gitlab.com/AndreyMZ/dst-more-equip-slots
[equip_types.lua]:        https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/src/scripts/more_equip_slots/equip_types.lua
[items_anim_symbols.lua]: https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/src/scripts/more_equip_slots/items_anim_symbols.lua
[items_equip_types.lua]:  https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/src/scripts/more_equip_slots/items_equip_types.lua
[README.md]:              https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/README.md
[CHANGELOG.md]:           https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/CHANGELOG.md
[mklinks.bat]:            https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/mklinks.bat
[merge request]:          https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/merge_requests
[issues]:                 https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues

[console]: https://dontstarve.fandom.com/wiki/Console
