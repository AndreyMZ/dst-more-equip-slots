set PROJECT_DIR=%~dp0

pushd "%PROJECT_DIR%lib\scripts"
wsl grep --recursive --line-number --perl-regexp "(?<! = )EQUIPSLOTS\.BODY" > "%PROJECT_DIR%docs\grep-equipslots.body-1.txt"
wsl grep --recursive --line-number --fixed-strings " = EQUIPSLOTS.BODY" > "%PROJECT_DIR%docs\grep-equipslots.body-2.txt"
popd

pause
