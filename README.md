This mod in Steam Workshop: <https://steamcommunity.com/sharedfiles/filedetails/?id=2324689937>.


Description
===========

## Motivation

Vanilla Don't Starve Together has one equipment slot "body" for all the following types of items:
- Heavy (sculpture pieces, Sunken Chest, etc.)
- Backpack
- Armor
- Dress
- Amulet

Once I found that I had been using backpacks and armors, but not dress and amulets. So, I had been missing a 
significant part of the game. I cannot refuse to use a backpack in favor of dress and/or amulets. Also, I still want
to switch between a backpack and an armor. So, I decided to add some new configurable equipment slots which may be used 
(optionally) for armors, dress, and amulets.

## Features

1.  In addition to the original body equipment slot add up to 3 extra slots! For each type of body items: armor, 
    dress, amulet - configure in which slot to equip it. For example:

    |               | Original slot    | Extra slot #1          | Extra slot #2  | Extra slot #3 |
    |---------------|------------------|------------------------|----------------|---------------|
    | **Example 1** | Backpack         | Armor                  | Dress          | Amulet        |
    | **Example 2** | Backpack         | Armor / Dress          | -              | Amulet        |
    | **Example 3** | Backpack         | Armor / Dress / Amulet | -              | -             |
    | **Example 4** | Backpack / Armor | -                      | Dress          | Amulet        |
    | **Example 5** | Backpack / Armor | -                      | Dress / Amulet | -             |

2.  Equipped body items are rendered as logical as possible given the constraints of the game:

    * Backpack is rendered on top of other items.
    * Armor covers dress.
    * Dress covers amulet.

## Differences with others similar mods

1. The codebase is smaller, and it is well written. This means that bugs (including compatibility bugs) are less 
   likely and easier to fix.
2. No obfuscated malicious code.   
3. No problems with backpacks, heavy items, One-man Band, and Snurtle Shell Armor by design.
4. No weird configuration options.
5. More useful configuration options! For example, you may have:
    * The same slot for backpacks and armors.
    * Different slots for armors and dress.


Compatibility
=============

Many mods add custom body items of different types. To work properly with this mod, such item should have a proper tag:
e.g. "heavy", "backpack", "armor", "dress", or "amulet". Otherwise, it will be equipped to the original body slot.

If you have encountered an incompatible mod and want to fix this, then please follow [the guide][].

[the guide]: https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/blob/master/docs/compatibility.md

## Known compatible mods

* [Aria Crystal (Rebuild)](https://steamcommunity.com/sharedfiles/filedetails/?id=2418617371)
* [Backpack In Inventory](https://steamcommunity.com/sharedfiles/filedetails/?id=1619310361)
* [Functional Medal](https://steamcommunity.com/sharedfiles/filedetails/?id=1909182187)
  
    Note: a separate slot for medals may be enabled in the configuration for the mod "Functional Medal", not "More equip
    slots".
  
* [Island Adventures](https://steamcommunity.com/sharedfiles/filedetails/?id=1467214795)
* [Item Auto-Fuel Slots](https://steamcommunity.com/sharedfiles/filedetails/?id=2736043172)
* [More Armor](https://steamcommunity.com/sharedfiles/filedetails/?id=1153998909)
* [Moving Box](https://steamcommunity.com/sharedfiles/filedetails/?id=1079538195)
* [{Shields} 2.0](https://steamcommunity.com/sharedfiles/filedetails/?id=2425399066)
* [[DST] WhaRang](https://steamcommunity.com/sharedfiles/filedetails/?id=1108032281)

## Questionable mods

Some items from these mods may be equipped properly if autodetecting armor and dress is switched on (in the settings for
"More equip slots"). But for some reason (e.g. source code obfuscation) I cannot check if the items function properly or
not.

* [Myth Words Theme-神话书说](https://steamcommunity.com/sharedfiles/filedetails/?id=1991746508) - obfuscated.
* [伊蕾娜](https://steamcommunity.com/sharedfiles/filedetails/?id=2640834455) - obfuscated.

## Known incompatible mods

* Other mods tweaking the equipment slots:
    - DoomofMax's [Extra Equip Slot Plus (+Render) [Doom]](https://steamcommunity.com/sharedfiles/filedetails/?id=2283028733)
    - Magic's [Extra Equip Slots](https://steamcommunity.com/sharedfiles/filedetails/?id=2075943614)
    - Jacob's [Backpack slot](https://steamcommunity.com/sharedfiles/filedetails/?id=1582457351)
    - [MX - Slot (Extra Equipment Slots)](https://steamcommunity.com/sharedfiles/filedetails/?id=2890837039)
* [璇儿XuanEr](https://steamcommunity.com/sharedfiles/filedetails/?id=2582670245) - obfuscated.


Limitations
===========

* If you are going to disable this mod, note the following. If when you exited the world last time your character 
  didn't have enough free inventory slots (+ the cursor slot) for some extra body items equipped, then after 
  disabling the mod and entering the world these items will be lost.
    
* When carrying a heavy item, other body equipment is not rendered.

* When One-man Band is equipped, other body equipment is rendered while moving, and it isn't while standing.


Report an issue
===============

Go to <https://gitlab.com/AndreyMZ/dst-more-equip-slots/-/issues>.
